﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppCore.Attributes
{
    /// <summary>
    /// Info about Entity for saving
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class EntityInfoAttribute : Attribute
    {
        public EntityInfoAttribute(string entityName, string scheme = null)
        {
            EntityName = entityName;
            Scheme = string.IsNullOrEmpty(scheme) ? "pms" : scheme;
        }

        public string EntityName { get; }

        public string Scheme { get; }
    }
}
