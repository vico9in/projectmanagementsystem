﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace AppCore.Helpers
{
    public static class ExtensionClass
    {
        public static T Extract<T>(this DataRow dr, string columnName)
        {
            if (dr.Table.Columns.Contains(columnName))
            {
                var val = dr[columnName];
                if (val is null || val is DBNull)
                    return default;
                else
                    return (T)val;
            }
            else
                return default;
        }
    }
}