﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AppCore.Entities;

namespace AppCore.Interfaces
{
    public interface IAsyncRepository<T> where T : BaseEntity
    {
        Task<T> GetByIdAsync(int id);

        Task<List<T>> ListAsync(Dictionary<string, object> spec);

        Task<JObject> AddAsync(T entity);

        Task<bool> UpdateAsync(T entity);

        Task DeleteAsync(T entity);
    }
}
