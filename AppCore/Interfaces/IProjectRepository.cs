﻿using AppCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AppCore.Interfaces
{
    public interface IProjectRepository : IAsyncRepository<ProjectCore>
    {
    }
}
