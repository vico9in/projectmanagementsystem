﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace AppCore.Entities
{
    public class TaskCore : BaseEntity
    {
        public TaskCore(DataRow dr) : base(dr)
        {
            Description = dr.Field<string>("description");
        }

        public string Description { get; }
    }
}