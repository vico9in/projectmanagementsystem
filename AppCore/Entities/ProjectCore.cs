﻿using AppCore.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace AppCore.Entities
{
    public class ProjectCore : BaseEntity
    {
        public ProjectCore(DataRow dr) : base(dr)
        {
            if (Enum.TryParse<ProjectCodeEnum>(dr.Field<string>("code"), out var code))
                Code = code;
            else
                throw new Exception("uknown project code");
                //other logic
        }
        public ProjectCodeEnum Code { get; }

        public string State { get; }
    }
}