﻿using AppCore.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace AppCore.Entities
{
    public abstract class BaseEntity
    {
        public BaseEntity()
        { }

        public BaseEntity(DataRow dr)
        {
            Id = dr.Field<int>("id");
            Name = dr.Field<string>("name");
            StartDate = dr.Field<DateTime>("startDate");
            FinishDate = dr.Field<DateTime>("finishDate");
        }

        public int Id { get; }

        public string Name { get; }

        public DateTime StartDate { get; }

        public DateTime FinishDate { get; }
    }
}