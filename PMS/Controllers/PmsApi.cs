﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace PMS.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PmsApi : ControllerBase
    {

        public PmsApi(ILogger<PmsApi> logger)
        {
            //_logger = logger;
        }

        
        [HttpGet]
        public async Task<string> Get()
        {
            return "ok";
        }
    }
}
