using AppCore.Entities;
using AppCore.Enums;
using NUnit.Framework;
using System;
using System.Data;

namespace UnitTests
{
    [TestFixture]
    public class Tests
    {
        DataRow _dataRow;
        ProjectCore _projectCore;
        DataTable _dataTable;
        int _projectCoreId = 125;
        string _projectCoreName = "";
        DateTime _projectCoreStartDate = new DateTime(2020, 01, 15);
        DateTime _projectCoreFinishDate = new DateTime(2020, 02, 15);
        ProjectCodeEnum _projectCoreCode = ProjectCodeEnum.B;

        [OneTimeSetUp]
        public void Setup()
        {
            _dataTable = new DataTable("dt");
            _dataTable.Columns.Add("id", typeof(int));
            _dataTable.Columns.Add("name", typeof(string));
            _dataTable.Columns.Add("startDate", typeof(DateTime));
            _dataTable.Columns.Add("finishDate", typeof(DateTime));
            _dataTable.Columns.Add("code", typeof(string));

            _dataRow = _dataTable.Rows.Add(_projectCoreId, _projectCoreName, _projectCoreStartDate, _projectCoreFinishDate, _projectCoreCode);            
        }

        [SetUp]
        public void EverSetup()
        {
            _projectCore = new ProjectCore(_dataRow);
        }

        [Test]
        public void CreateProjectCore_General()
        {
            Assert.IsNotNull(_projectCore);
        }

        [Test]
        public void CreateProjectCore_Id()
        {
            Assert.AreEqual(_projectCoreId, _projectCore.Id);
        }

        [Test]
        public void CreateProjectCore_StarDate()
        {
            var projectCore = new ProjectCore(_dataRow);

            Assert.AreEqual(_projectCoreStartDate, _projectCore.StartDate);
        }

        [Test]
        public void CreateProjectCore_FinishDate()
        {
            var projectCore = new ProjectCore(_dataRow);

            Assert.AreEqual(_projectCoreFinishDate, _projectCore.FinishDate);
        }

        [Test]
        public void CreateProjectCore_Code()
        {
            var projectCore = new ProjectCore(_dataRow);

            Assert.AreEqual(_projectCoreCode, _projectCore.Code);
        }

        [Test]
        public void CreateProjectCore_CodeUknown()
        {
            var dr = _dataTable.Rows.Add(_projectCoreId, _projectCoreName, _projectCoreStartDate, _projectCoreFinishDate, "none");

            Assert.Throws<Exception>(() => new ProjectCore(dr));
        }
    }
}