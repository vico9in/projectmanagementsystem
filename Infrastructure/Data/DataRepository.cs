﻿using AppCore.Attributes;
using AppCore.Entities;
using AppCore.Helpers;
using AppCore.Interfaces;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Data
{
    public abstract class DataRepository<T> : IAsyncRepository<T> where T : BaseEntity
    {
        private readonly string _connectionString;
        public DataRepository()
        {
            _connectionString = RepositorySettings.ConnectionString;
        }

        public Task<JObject> AddAsync(T entity)
        {
            return Task.Run(() => { return Add(entity); });
        }

        public async Task<T> GetByIdAsync(int id)
        {
            DataTable dt = null;
            var entityInfo = GetEntityInfo();

            using (var connection = new SqlConnection(_connectionString))
            {
                try
                {
                    using (var command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = $"[{entityInfo.scheme}].[Get{entityInfo.entityName}]";
                        command.Parameters.Clear();
                        command.Parameters.AddWithValue("id", id);

                        await connection.OpenAsync();

                        //execute sql
                        using (var adapter = new SqlDataAdapter(command))
                        {
                            await Task.Factory.StartNew(() =>
                            {
                                dt = new DataTable();
                                adapter.Fill(dt);
                            });
                        }
                    }
                }
                finally
                {
                    if (connection != null && connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                    connection.Dispose();
                }
            }

            if (dt != null)
            {
                var row = dt.Rows[0];
                var res = (T)Activator.CreateInstance(typeof(T), row);
                return res;
            }
            else
                return null;
        }

        public Task<List<T>> ListAsync(Dictionary<string, object> spec, string procName = null)
        {
            return Task.Run(() => { return List(spec, procName); });
        }

        public Task<bool> UpdateAsync(T entity)
        {
            return Task.Run(() => { return Update(entity); });
        }

        public Task DeleteAsync(T entity)
        {
            throw new NotImplementedException();
        }

        private JObject Add(T entity)
        {
            DataTable dataTable = null;
            var entityInfo = GetEntityInfo();

            using (var connection = new SqlConnection(_connectionString))
            {
                try
                {
                    using (var command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = $"[{entityInfo.scheme}].[Add{entityInfo.entityName}]";
                        command.Parameters.Clear();
                        command.Parameters.AddRange(GetParams(entity));
                        connection.Open();

                        using (var adapter = new SqlDataAdapter(command))
                        {
                            dataTable = new DataTable("Table1");
                            adapter.Fill(dataTable);
                        }
                    }
                }
                finally
                {
                    if (connection != null && connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                    connection.Dispose();
                }
            }

            var newItem = (T)Activator.CreateInstance(typeof(T), dataTable.Rows[0]);

            return JObject.FromObject(newItem);
        }

        private List<T> List(Dictionary<string, object> spec, string procName = null)
        {
            DataSet dataSet;
            var entityInfo = GetEntityInfo();

            using (var connection = new SqlConnection(_connectionString))
            {
                try
                {
                    using (var command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = procName ?? $"[{entityInfo.scheme}].[Get{entityInfo.entityName}s]";
                        command.Parameters.Clear();

                        if (spec != null)
                        {
                            foreach (var row in spec)
                                command.Parameters.AddWithValue(row.Key, row.Value != null ? row.Value : DBNull.Value);
                        }

                        connection.Open();

                        //execute sql
                        using (var adapter = new SqlDataAdapter(command))
                        {
                            dataSet = new DataSet();
                            adapter.Fill(dataSet);
                        }
                    }
                }
                finally
                {
                    if (connection != null && connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                    connection.Dispose();
                }
            }

            var res = new List<T>();
            var dataTable = dataSet.Tables[0];
            var dt2 = dataSet.Tables.Count > 1 ? dataSet.Tables[1].AsEnumerable() : null;

            foreach (DataRow row in dataTable.Rows)
            {
                var newItem = (T)(dt2 is null ? Activator.CreateInstance(typeof(T), row) : Activator.CreateInstance(typeof(T), row, dt2));
                res.Add(newItem);
            }

            return res;
        }

        private SqlParameter[] GetParams(T entity)
        {
            var res = new List<SqlParameter> { };
            var props = typeof(T).GetProperties();
            foreach (var p in props)
            {
                var value = p.GetValue(entity);
                if (value != null)
                    res.Add(new SqlParameter(p.Name, value));
            }
            return res.ToArray();
        }

        private bool Update(T entity)
        {
            int res = 0;
            var entityInfo = GetEntityInfo();

            using (var connection = new SqlConnection(_connectionString))
            {
                try
                {
                    using (var command = new SqlCommand())
                    {
                        command.Connection = connection;
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.CommandText = $"[{entityInfo.scheme}].[Update{entityInfo.entityName}]";
                        command.Parameters.Clear();
                        command.Parameters.AddRange(GetParams(entity));

                        connection.Open();

                        res = command.ExecuteNonQuery();
                    }
                }
                finally
                {
                    if (connection != null && connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                    connection.Dispose();
                }
            }

            return res > 0;
        }

        private (string entityName, string scheme) GetEntityInfo()
        {
            var attributes = typeof(T).GetCustomAttributes(false).ToList();
            var entityInfoAttribute = attributes.First(a => a is EntityInfoAttribute);
            var entityInfo = entityInfoAttribute as EntityInfoAttribute;
            return (entityInfo.EntityName, entityInfo.Scheme);
        }

        public Task<List<T>> ListAsync(Dictionary<string, object> spec)
        {
            throw new NotImplementedException();
        }
    }
}
