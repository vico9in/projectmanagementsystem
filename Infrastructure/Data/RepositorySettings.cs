﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Data
{
    public static class RepositorySettings
    {
        internal static string ConnectionString { get; private set; }

        public static void Init(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString))
                ConnectionString = connectionString;
        }
    }
}
