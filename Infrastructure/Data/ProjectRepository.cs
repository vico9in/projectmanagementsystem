﻿using AppCore.Entities;
using AppCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infrastructure.Data
{
    public class ProjectRepository : DataRepository<ProjectCore>, IProjectRepository
    {
    }
}
